# Plutio Developer Skill Test

This test is comprised of 3 parts based on a basic Meteor app. All 3 parts of the test must be completed specifically as instructed without anything additional.

Feel free to use any npm modules. Start the app by running `npm start`. When you are done, zip all files except `.meteor` and `node_modules` and send via job application form.

Good luck.

## Part 1

On a "Update data" button click, get 15 articles from https://api.spaceflightnewsapi.net/v3/documentation (in `imports/api/posts/methods/fetch-data.js`) and insert to `Posts` collection having data defined in `Posts` schema in `imports/api/posts/model/schema.js`.

- It should not insert duplicate posts when refetching.
- After post is removed by clicking "Remove" button, fetching again should not insert the removed article. Implement the logic to handle removed posts.

## Part 2

Implement method for fetching a single article by ID in `imports/api/posts/methods/fetch-data.js` `fetchPost`.

## Part 3

Write a test of `fetch-data` method inside `tests` folder. You can run tests with `npm run test`.
