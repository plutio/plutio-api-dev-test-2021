import { Meteor } from 'meteor/meteor';
import { Posts } from '/imports/api/posts';

const testData = [
  {
    title: 'South Korea’s air force opens space ops center',
    url: 'https://spacenews.com/south-korean-air-force-opens-space-center/',
    imageUrl:
      'https://spacenews.com/wp-content/uploads/2021/10/210930154346860.jpg',
    publishedAt: new Date()
  }
];

Meteor.startup(() => {
  if (Posts.find().count() === 0) {
    testData.forEach(post => Posts.insert(post));
  }
});
