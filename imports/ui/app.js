import React, { useEffect, useState } from 'react';

const App = () => {
  const [posts, setPosts] = useState([]);
  const [fetchKey, setFetchKey] = useState(null);

  useEffect(() => {
    Meteor.call('getPosts', (err, res) => {
      if (!err && res) {
        setPosts(res);
      }
    });
  }, [fetchKey]);

  const handleRemove = postId => {
    Meteor.call('removePost', postId, err => {
      if (!err) {
        setPosts(posts.filter(post => post._id !== postId));
      }
    });
  };

  const handleFetch = () => {
    Meteor.call('fetchPosts', err => {
      if (!err) {
        setFetchKey(Date.now());
      }
    });
  };

  return (
    <div className="page">
      <div className="page-header">
        <div className="content has-back-button">
          <div className="content-block">
            <a onClick={handleFetch}>Update data</a>
          </div>
          <div className="content-block">
            <div className="page-title">
              <h1>Posts</h1>
            </div>
          </div>
        </div>
      </div>
      <div className="main-tasks-page content list-view">
        <div className="groups-wrapper">
          <div className="group-container open">
            <div className="group-head">
              <p>Posts</p>
            </div>
            <div className="group-content">
              {posts.map(post => (
                <div key={post._id} className="task">
                  <div className="task-link has-color-tag" href={post.url}>
                    <div className="task-link-head">
                      <div className="task-title">
                        <a>{post.title}</a>
                      </div>
                    </div>
                    <div className="task-link-body">
                      <div className="indicators">
                        <span className="icon-indicator">
                          Posted at: {post.publishedAt.toISOString()}
                        </span>
                        <span
                          className="icon-indicator"
                          style={{ marginLeft: '20px', color: 'red' }}
                        >
                          <a onClick={handleRemove.bind(null, post._id)}>
                            Remove
                          </a>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default App;
