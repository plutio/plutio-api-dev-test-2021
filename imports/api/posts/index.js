import './methods/get';
import './methods/remove';
import './methods/fetch-data';

export { Posts } from './model/schema';
