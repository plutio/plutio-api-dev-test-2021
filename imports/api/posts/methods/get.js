import { Meteor } from 'meteor/meteor';
import { Posts } from '../';

Meteor.methods({
  getPosts: () => {
    return Posts.find({}, { sort: { createdAt: -1 } }).fetch();
  }
});
