import { Meteor } from 'meteor/meteor';
import { Posts } from '../';

Meteor.methods({
  removePost: postId => {
    return Posts.remove({ _id: postId });
  }
});
