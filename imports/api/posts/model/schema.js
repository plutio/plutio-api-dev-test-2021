import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Posts = new Mongo.Collection('plutioTestPosts');

const regExId = SimpleSchema.RegEx.Id;

Posts.schema = new SimpleSchema(
  {
    _id: {
      type: String,
      regEx: regExId
    },
    createdAt: {
      type: Date,
      autoValue() {
        if (this.isInsert) {
          return new Date();
        }
      }
    },
    updatedAt: {
      type: Date,
      autoValue() {
        if (this.isUpdate) {
          return new Date();
        }
      }
    },
    title: String,
    url: String,
    imageUrl: String,
    publishedAt: Date
  },
  {
    clean: {
      filter: true,
      autoConvert: true,
      removeEmptyStrings: false,
      trimStrings: false,
      getAutoValues: true,
      removeNullsFromArrays: true
    },
    requiredByDefault: false
  }
);

Posts.attachSchema(Posts.schema);
